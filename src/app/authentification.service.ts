import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable , of, Subject, timer } from 'rxjs';
import { catchError, map, tap, timeout } from 'rxjs/operators';
import { LoginComponent } from './login/login.component';
import { SliderComponent } from './slider/slider.component';
import { User } from './user';
import { Profile } from './Profile';





@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

 private  apiUrl = 'http://localhost:8084/api/v1/users';
 private  apiUrl2 = 'http://localhost:8084/api/v1/profiles';

  hide:boolean;
  private booleanValue=new Subject<boolean>();
  booleanimport=this.booleanValue.asObservable();
  public id:number;



  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private http: HttpClient, private route: ActivatedRoute,
    private router: Router ) { }

  signup(formData: NgForm) {
    return this.http.post<any>( `${this.apiUrl}`, formData).pipe(
      tap(user => {
        console.log(user);
      }),
      catchError(this.handleError('profile', []))
    );
  }

  login(user:User):Observable<User[]> {
   const lognUrel=`${this.apiUrl}`
   return this.http.get<getResponseUser>( lognUrel).pipe(
      timeout(3000),
     map(response=>response._embedded.users),
    /*  tap(user => {
        if (user ) {
          localStorage.setItem('user', JSON.stringify(user));
        }

      }),
     // catchError(this.handleError('', []))*/
    );
    
    
  }

  logout() {
    if (localStorage.getItem('user')) {
      localStorage.removeItem('user');
      this.router.navigate(['/']);
    }
  }

  isloggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }
 
 
 
  getUserProfile(id :number):Observable<Profile[]> {
  /*  this.slidCOmponent.longimport.subscribe({
      next:value=>this.id=value
     } )*/
    const profileUrl=`${this.apiUrl2}/search/findById?id=${id}`;
    return this.http.get<getResponseProfile>( profileUrl).pipe(
      
      map( response=>response._embedded.profiles),

    );
     
 
  }
  
 
  toggleSidebar(val :boolean ) {
  val=!val;
  this.booleanValue.next(val);
  return  val;
    
  }

}
interface getResponseUser{
  _embedded:{
    users:User[];
  }
  
 
}
interface getResponseProfile{

  
  _embedded:{
    profiles:Profile[];
  }
  
  
}
