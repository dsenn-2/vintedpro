import { Component, Input, OnInit, Output } from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Profile } from '../Profile';
import { profile } from 'console';
import * as Long from 'long';
import { Observable, Subscriber, timer } from 'rxjs';
import { timeout } from 'rxjs/operators';






@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loginForm: FormGroup;
  public  profileId;
  profiles:Profile[];
  profile=new Profile();
  msg="";
  public values ;
 /*  id:String  ;
   last_name:String  ;
   first_name:String  ;
   picture:String  ;
   iaddressd:String  ;
   username:String  ;
   password:String  ;*/
  

  subscriber:Observable<Profile[]>;
 

  constructor(private auth:AuthentificationService,private http: HttpClient,private fb: FormBuilder,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      'email' : [null],
      'password' : [null],
      'repeatpassword' : [null],
    });
    
    this.values =JSON.parse(localStorage.getItem('user'));
   
     console.log(this.values['id']);

   
   
   
   this.profile;
   this.profiles=[];
  
  // this.getProfile();
  /* this.route.paramMap.subscribe(
     
    ()=>{
       
       this.getProfile();
     }
   );*/
   
  }
  
  opened=false;
  selectedFIle=null;


  @Output() toggle(){
  
  this.auth.booleanimport.subscribe({
   next: value=>this.opened=value
   
  });
  
  return !this.opened
     }
     onFIleSelected(event){
       console.log(event);
       this.selectedFIle=event.target.files[0];

     }
     onUpload(){
      const id:number=+this.route.snapshot.paramMap.get('id');
      this .subscriber=this.auth.getUserProfile(id) ;
    console.log(this.subscriber) }
     getProfile(){
     
      const id:number=+this.route.snapshot.paramMap.get('id');
      this.auth.getUserProfile(id ).subscribe( 
         response=>console.log(response)
        );
         
      
      
      
}
}
