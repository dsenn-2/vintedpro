import { Component, OnInit } from '@angular/core';
import { SidebarModule } from 'ng-sidebar';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { AuthentificationService } from '../authentification.service';
import { Observable } from 'rxjs';
import { nextTick } from 'process';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  opened=false;

  constructor( private auth:AuthentificationService) { }

  ngOnInit(): void {
   this.toggle();
    
  }
 toggle(){
  
 this.auth.booleanimport.subscribe({
  
  next: value=>this.opened=value
  
 });
 
 return this.opened
    }
  
 

}
