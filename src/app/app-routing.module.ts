import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ PortfolioComponent} from './portfolio/portfolio.component';
import { AboutComponent } from './about/about.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PricingComponent } from './pricing/pricing.component';
import { SaidebarComponent } from './saidebar/saidebar.component';
import { ClientComponent } from './client/client.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { TestimontialComponent } from './testimontial/testimontial.component';
import { LoginComponent } from './login/login.component';
import { GaurdService } from './gaurd.service';
import { SignupComponent } from './signup/signup.component';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';





const routes: Routes = [
  {path:'',redirectTo:'navbar',pathMatch:'full'},
  //{path:'',component:AppComponent},
  {path:'signup',component:SignupComponent},
  {path:'profile/:id',component:ProfileComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
