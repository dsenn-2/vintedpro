import { Component, OnInit,Input, Output ,EventEmitter} from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl, RequiredValidator} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { Observable , of, Subject } from 'rxjs';
import { element } from 'protractor';
import { User } from '../user';
import { AngularFirestoreModule } from '@firebase/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { debounceTime,map ,take, timeout } from 'rxjs/operators';
import { Profile } from '../Profile';
import { idText } from 'typescript';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  providers:[]

})
export class SliderComponent implements OnInit {
  @Input()loginForm: FormGroup;
  @Input()returnUrl: string;
  @Input() static submitted=false;
  pageName:String;
  loggedIn=false;
   users:User[];
   user=new User ();
   profile=new Profile();
   id:number ;
 
   msg="";
   private longValue=new Subject<number>();
   longimport=this.longValue.asObservable();

  constructor(private fb: FormBuilder,
    private auth: AuthentificationService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
    ngOnInit() {
      this.loginForm = this.fb.group({
        'email' : [null,Validators.required],
        'password' : [null,Validators.required],
        'repeatpassword' : [null,Validators.required]
      });
  
      this.id;
      this.users=[];
      this.login();
     
      
  
    }
  opened =false;
 

 
 
  login(){
    

    
    this.auth.login(this.user).subscribe(
     data=>this.users=data);
     
     
     for( let  i=0 ;i<this.users.length;i++){
      if(this.users[i].password==this.loginForm.get('password').value&&this.users[i].username==this.loginForm.get('email').value)
      {
        this.opened=this.auth.toggleSidebar(this.opened);
        this.router.navigate(['/profile',this.users[i].id]);
      // this.longValue.next(this.users[i].id);
       localStorage.setItem('user', JSON.stringify(this.users[i]) );
    
      }
      else{
        this.msg=" Invalid Email or Password";
      }
     }
    
  }
 
  signup(formData: NgForm) {
    return this.auth.signup(formData).subscribe(
      (user) => {
        console.log(`response received`);
        this.router.navigate(['/profile']);
      });
  }
 @Output() public  childevent = new EventEmitter();
}
/*export class CustomValidator{
  static username(afs :AngularFirestore){
    return (control:AbstractControl)=>{
      const username =control.value.toLowerCase();
      return afs.collection('users', ref=>ref.where('username', '==' ,username))
      .valueChanges().pipe(
        debounceTime(500),
        take(1),
        map(arr=>arr.length ? {usernameAvailable:false}:null),
      )
    }
  }
}
*/