import { Component, Input, Output } from '@angular/core';
import { AuthentificationService } from './authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private auth:AuthentificationService ) {  }
  title = 'testProject';
  public opened=false ;

   toggle(){
  
    this.auth.booleanimport.subscribe({
     next: value=>this.opened=value
     
    });
    
    return this.opened
       }

}
