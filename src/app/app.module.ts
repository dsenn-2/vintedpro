import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PricingComponent } from './pricing/pricing.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { SaidebarComponent } from './saidebar/saidebar.component';
import { MailActionComponent } from './mail-action/mail-action.component';
import { TestimontialComponent } from './testimontial/testimontial.component';
import { ClientComponent } from './client/client.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MaterialModule } from './material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from 'ng-sidebar';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './signup/signup.component';
import { FormsModule } from '@angular/forms';
import * as $ from 'jquery';
import{Router,RouterModule} from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SliderComponent } from './slider/slider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { config } from 'process';





@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AboutComponent,
    NavbarComponent,
    PricingComponent,
    PortfolioComponent,
    SaidebarComponent,
    MailActionComponent,
    TestimontialComponent,
    ClientComponent,
    ContactComponent,
    FooterComponent,
    NavComponent,
    SidebarComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    SliderComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
     ReactiveFormsModule,
     HttpClientModule,
     AngularFireModule.initializeApp(config),
     AngularFirestoreModule, // firestore
     AngularFireAuthModule, // auth
     AngularFireStorageModule ,// storage
    SidebarModule.forRoot(),
    RouterModule,
   
    NgbModule

  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
